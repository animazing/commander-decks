## Introduction

Make peace not war. Let's see if we can win without doing combat damage to our opponents.

We center our tactics on three mechanics, two of which our commander has a direct influence on. 

The win conditions are:

* Life gain: [[Celestial Convergence]], [[Felidar Sovereign]] and [[Test of Endurance]]
* Draw: [[Approach of the Second Sun]]
* Treasure/Artifacts: [[Revel in Riches]] and [[Mechanized Production]]. 

## Life gain
Kenrith has a build in life gain effect which should make this quite easy to do. I added some creatures like [[Archangel of Thune]] and [[Heliod, Sun-Crowned]] which helps with both life gain but also makes me add a lot of +1/+1 counters for my other win cons. 

## Draw
Right now only one card gives me the win with card draw [[Approach of the Second Sun]]. However there are some good cards to support it, besides Kenrith's build-in draw ability. [[Lim-Dul's Vault + Sensei's Divining Top]] can trigger the win for minimum mana. [[Blue Sun's Zenith]] is an other quick win if I have the mana to spare.

## Treasure & Artifacts
Even if I have a bit of a slow game using [[Anointed Procession + Dockside Extortionist]] when I have either [[Revel in Riches]] or [[Mechanized Production]] in would result in one round win, or insta win if I have a flash enabler online. In a four player game [[Smothering Tithe]] might net me three treasures in a good round so the wincon seems easy to get to. Treasures also help with the mana heavy abilities of my commander so they are always a good thing to have even if it does not win me the game. However I can't directly influence the treasures with my commander. I do feel it might be a stronger option than the counters option since I don't run that many creatures.

I also threw [[Happily Ever After]] in since it might be easy to win that by accident in a 5 color deck.

## Playstyle
The idea is to entice my opponents by using Kenrith's, heavily discounted (see Discount category) abilities to steer them away from me while working on getting my alternative wins setup. To further prevent them from attacking me I added a few [[Ghostly Prison]] effects. Curses (such as [[Curse of Bounty]]) should help me entice my opponents to attack each other.

## Some notable cards

[[Alhammarret's Archive]] will help me out with both the draw & life wincons and is an overall great card to have. 

[[Rings of Brighthearth]] will trigger Kenrith's abilities twice. 

[[Happily Ever After + Conflux]] seems like an interesting combination to quickly ensure different colors permanents are in play.

[[Aetherflux Reservoir]] could work as a great alternative finisher

[[Heliod's Intervention + Alhammarret's Archive]] Would mean that for ten mana I could gain 32 life, a good way to surprise into a 50 life victory. 

[[Spell Swindle]] Could be a great finisher as well if you can get it to counter a big spell while [[Mechanized Production]] or [[Revel in Riches]] is already in play.

[[Seedborn muse]] is used to make sure Kenrith is available in everybody's turn to either play politics or boost your own life or creatures.
